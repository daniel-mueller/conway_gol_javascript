
function printMatrix(matrix) {

    var matrixString = "";
    for (i = 0; i < 30; i++) {
        matrixString += matrix[i].toString() + "<br />";
    }

    document.getElementById("output").innerHTML = matrixString;
}

//Wird auf Matrix angewendet um zu bestimmen, was in diesem Schritt geschieht
function applyRules(matrix1, matrix2) {
    //Jedes Feld muss einzeln behandelt werden
    //Wenn ein Feld behandelt wird, werden erst Nachbarn gezählt
    //Dann werden die Regeln angewendet auf Anzahl Nachbarn & momentane Wertigkeit
    var nachbarn;

    for (i = 0; i < 30; i++) {
        for (j = 0; j < 30; j++) {

            nachbarn = 0;

            //Anzahl der Nachbarn
            //Abfrage ob undefined, weil Javascript sonst bockt
            //Diese muss aufgeteilt werden, da man anscheinend keine multidimensionalen Arrays auf undefined überprüfen kann
            if (matrix1[i-1] != undefined) {
                if (matrix1[i-1][j-1] == "&bull;") {nachbarn++;}
                if (matrix1[i-1][j] == "&bull;") {nachbarn++;}
                if (matrix1[i-1][j+1] == "&bull;") {nachbarn++;}
            }

            if (matrix1[i][j-1] == "&bull;") {nachbarn++;}
            if (matrix1[i][j+1] == "&bull;") {nachbarn++;}

            if (matrix1[i+1] != undefined) {
                if (matrix1[i+1][j-1] == "&bull;") {nachbarn++;}
                if (matrix1[i+1][j] == "&bull;") {nachbarn++;}
                if (matrix1[i+1][j+1] == "&bull;") {nachbarn++;}
            }
            //Anwenden der Regeln

            if      (matrix1[i][j] == "&bull;" && nachbarn < 2) {matrix2[i][j] = "_";}
            else if (matrix1[i][j] == "&bull;" && nachbarn > 3) {matrix2[i][j] = "_";}
            else if (matrix1[i][j] == "&bull;" && (nachbarn == 2 || nachbarn == 3)) {matrix2[i][j] = "&bull;";}
            else if (matrix1[i][j] == "_" && nachbarn == 3) {matrix2[i][j] = "&bull;";}

            //Matrix2 enthält jetzt den nächsten Zustand des Spiels
        }
    }
}

//löscht ein Feld bzw. setzt Feld überall auf 0
function emptyField(matrix) {
    //for...in vermeiden bei Arrays
    for (i = 0; i < 30; i++) {
        for (j = 0; j < 30; j++) {
            matrix[i][j] = "_";
        }
    }
}


function applyInput(matrix,x,y) {
    //alert('test')
    //Arrayzählung beginnt mit 0
    x = x - 1;
    y = y - 1;
    if (matrix[x][y] == "&bull;") {
        matrix[x][y] = "_";
    }

    else {
        matrix[x][y] = "&bull;";
    }

    printMatrix(matrix);
}

//Schreibt die neue Matrix über die Alte
function swapFields(matrix1, matrix2) {

    for (i = 0; i < 30; i++) {
        for (j = 0; j < 30; j++) {
            matrix1[i][j] = matrix2[i][j];
        }
    }
}

function nextStep(matrix1, matrix2) {

    applyRules(matrix1,matrix2);
    swapFields(matrix1,matrix2);
    printMatrix(matrix1);

}

function unlimited(matrix1,matrix2) {

    window.setInterval("nextStep(matrix1,matrix2)", 600);
}

function demo1(matrix1,matrix2) {
    applyInput(matrix1,2,6);
    applyInput(matrix1,3,6);
    applyInput(matrix1,4,6);

    applyInput(matrix1,8,8);
    applyInput(matrix1,8,9);
    applyInput(matrix1,9,8);
    applyInput(matrix1,11,11);
    applyInput(matrix1,10,11);
    applyInput(matrix1,11,10);

    applyInput(matrix1,22,24);
    applyInput(matrix1,22,25);
    applyInput(matrix1,22,26);
    applyInput(matrix1,21,26);
    applyInput(matrix1,20,25);

    applyInput(matrix1,16,16);
    applyInput(matrix1,16,17);
    applyInput(matrix1,16,18);
    applyInput(matrix1,15,18);
    applyInput(matrix1,14,17);

    unlimited(matrix1,matrix2);
}

function demo2(matrix1,matrix2) {
    applyInput(matrix1,4,15);
    applyInput(matrix1,4,16);
    applyInput(matrix1,4,17);
    applyInput(matrix1,5,15);
    applyInput(matrix1,5,17);
    applyInput(matrix1,6,15);
    applyInput(matrix1,6,17);

    applyInput(matrix1,9,15);
    applyInput(matrix1,9,16);
    applyInput(matrix1,9,17);
    applyInput(matrix1,8,15);
    applyInput(matrix1,8,17);
    applyInput(matrix1,7,15);
    applyInput(matrix1,7,17);

    unlimited(matrix1,matrix2);
}

function demo3(matrix1,matrix2) {
    applyInput(matrix1,14,14);
    applyInput(matrix1,15,14);
    applyInput(matrix1,16,14);
    applyInput(matrix1,14,15);
    applyInput(matrix1,15,13);

    unlimited(matrix1,matrix2);
}


